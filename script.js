process.stdin.setEncoding('utf-8');

process.stdin.on('readable', function(){
    var lang = process.env.LANG;
    var version = process.versions.node;
    var OS = process.platform;

    switch (OS) {
        case "linux":
        process.stdout.write("Operating system: " + OS + ", user's language: " + lang + ", node.js version: " + version + " ");
        break;
        case "darwin": 
        process.stdout.write("Operating system: " + OS + ", user's language: " + lang + ", node.js version: " + version + " ");
        break;
    }
    /*
    var input = process.stdin.read();
    if (input !== null) {
        var instruction = input.toString().trim();
        if (instruction == '/exit') {
            process.stdout.write('Quitting app!\n');
            process.exit();
        } else {
            process.stderr.write('Wrong instruction!\n');
        }
    }*/
});